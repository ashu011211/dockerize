FROM maven as build
WORKDIR /war-build/
RUN git clone https://gitlab.com/ashu011211/dockerize.git
ARG https_proxy
ENV https_proxy=${https_proxy}
RUN mvn -f dockerize/pom.xml package

FROM tomcat
WORKDIR /usr/local/tomcat/webapps/
COPY --from=build /war-build/dockerize/target/*.war .