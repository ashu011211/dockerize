#!/usr/bin/env bash
#git clone https://gitlab.com/ashu011211/dockerize.git
#cd dockerize
# if having git, repo can be cloned using above command
echo -e "\e[1;33m==============Building WAR File===================\e[0m"
docker build --no-cache . -t dockerized:1.0
echo -e "\e[1;33m==============Starting Application====================\e[0m"
docker run -d -p 9090:8080 dockerized:1.0
echo -e "\e[1;36m===================================================\e[0m"
echo -e "Application is deployed to port 9090"
echo -e "\e[1;32m===================================================\e[0m"
echo -e "Open localhost:9090/hello-world to access"
echo -e "\e[1;32m===============Deployment completed===================\e[0m"
